## 1. Introduction to CSS
In the previous session we understood how to write a HTML file and what are some basic tags to use. Note that we coded in the file **index.html**. Now, let's see how to put some life to that black and while page. Basically, I mean styling!!

#### CSS: Cascading Style Sheets
Cascading Style Sheets (CSS) is a stylesheet language used to describe the presentation of a document written in HTML. CSS describes how elements should be rendered on screen, or on other media.
Yes!! Let's start coding some CSS. Before that, we need to link a CSS file to the **index.html** page.

## 2. How to link a CSS file to HTML?

A **good** coding practice is to always keep any CSS files in a separate folder under your project. So, right click on the **project** folder in your code editor and create a folder called **CSS**. Inside the CSS folder, create a new life as **index.css**.
Your file structure should look like this now:

![alt](https://www.linkpicture.com/q/ruchika-sc-at-25-05-2021-00-38-36.png)

##### The `<link>` tag
Open your **index.html** file. Paste the following line in the head (between the `<head>` and `</head>` tags):
```sh
<link rel="stylesheet" href="css/index.css">
```
What did we do here?
- A `<link>` is used to link the CSS file to the HTML file. This will **apply** the css to your HTML document. Yes,  a style is applied to the HTML. We are not yet done!!
- The `rel` attribute shorthand to relation. A link relation is a descriptive attribute attached to a hyperlink in order to define the type of the link, or the relationship between the source and destination resources. Here, the `rel` attribute says that the linked file is a `stylesheet`. `rel="stylesheet"` is a mandatory attribute.
- `href` attribute like we saw on `<img>` holds the path to the css file. As you have seen in out project, the css file is inside the CSS folder so we right `css/index.css`.

Now your `index.html` should look like this(continuing from previous lesson's html):
```sh
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>My test page</title>
    <link rel="stylesheet" href="css/index.css">
  </head>
  <body>
    <p>Hello World!</p>
    <p> A cat belongs to the tiger family. </p>
    <a href="https://example.com">Website</a>
    <img width="500" height="500" src="https://www.rd.com/wp-content/uploads/2021/01/GettyImages-588935825.jpg"/>
    <ul>
    <li>Milk</li>
        <li>Cheese
            <ul>
                <li>Blue cheese</li>
                <li>Feta</li>
            </ul>
        </li>
    </ul>
    <ol>
        <li>Mix flour, baking powder, sugar, and salt.</li>
        <li>In another bowl, mix eggs, milk, and oil.</li>
        <li>Stir both mixtures together.</li>
        <li>Fill muffin tray 3/4 full.</li>
        <li>Bake for 20 minutes.</li>
    </ol>
    <table border="1">
        <tr>
            <th colspan="2">The table header</th>
        </tr>
        <tr>
            <td>The table body</td>
            <td>with two columns</td>
        </tr>
    </table>
  </body>
</html>
```

## Your first CSS ruleset

Yay!! Let's write some CSS code.
Go to the `index.css` file in your project and write:
```sh
p {
    color: red;
}
```
What do you see???
****
****
![alt](https://www.linkpicture.com/q/ruchika-sc-at-25-05-2021-00-59-22.png)
****
****
If you see, the first two lines are in red!!
Let's dissect the CSS code for red paragraph text to understand how it works :

- The whole structure is called a ruleset.
- **Selector** This is the HTML element name at the start of the ruleset. It defines the element(s) to be styled (in this example, `<p>` elements). To style a different element, change the selector.
- **Declaration** This is a single rule like `color: red;`. It specifies which of the element's properties you want to style.
- **Properties** These are ways in which you can style an HTML element. (In this example, color is a property of the <p> elements.) In CSS, you choose which properties you want to affect in the rule.
- **Property value** To the right of the property—after the colon—there is the property value. This chooses one out of many possible appearances for a given property. (For example, there are many color values in addition to red.) Try your favorite color.

Note the other important parts of the syntax: 
Apart from the selector, each ruleset must be wrapped in curly braces. ({})
- Within each declaration, you must use a colon `(:)` to separate the property from its value or values.
- Within each ruleset, you must use a semicolon `(;)` to separate each declaration from the next one.

#### Some more examples:
Add the following code to index.css
```sh
p {
  color: red;
  width: 500px;
  border: 1px solid black;
}
```
**Output**
****
****
![alt](https://www.linkpicture.com/q/ruchika-sc-at-25-05-2021-01-09-31.png)
****
****
You should see a border and width to the `<p>` element with a border.
- A `width` property defined the width of element. Here, it is `500px` which means, the `<p>` element has a width of `500px`. Note that `px` means *pixels* a measuring unit.
- The `border` proprty defined a border with values `1px solid black` = **"1px thickness of solid type black colored border"**

## 4. Different types of selectors

1] **Element selector (sometimes called a tag or type selector)**: It selects all HTML elements of the specified type.

Selecting a `<p>` tag:
```sh
p {
  color: red;  
}
```
Selecting a `<body>` tag:
```sh
body {
    background-color: yellow;
}
```
Selecting a `<h1>` tag:
```sh
h1 {
    color: green;
}
```
Try editing your index.css with the above styles. It should look like this:
The page background becomes yellow color and "Hello World!" in green!! Yeh!!
****
****
![alt](https://www.linkpicture.com/q/ruchika-sc-at-25-05-2021-01-27-13.png)
****
****
2] **ID selector**: It selects an element on the page with the specified ID. On a given HTML page, each id value should be unique.
```sh
<p id="my-id">
```
- Observe the rule of writing an ID in html file. `id="id_name"`. Here, `my-id`.
- Written only after the open brackets, `<p` and before closing `>`

3] **Class selector**: Selects the element(s) on the page with the specified class. Multiple instances of the same class can appear on a page.
```sh
<p class="my-class">
<a class="my-class">
```
- Observe the rule of writing a class. `class="my-class"`
- In the example, there is `<p>` and `<a>` tags having same `class` names.

Too much to understand `ID`s and `class`s?? Let's simply understand with a real life example.

#### A School

Open a new html file under your project as **selectors.html**. Your file structure should look like this now: 
![alt](https://www.linkpicture.com/q/ruchika-sc-at-25-05-2021-01-39-21.png)

Copy this full html code to selectors.html file which has the same index.css linked like in index.html:
```sh
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>A school</title>
  <link rel="stylesheet" href="css/index.css">
</head>
<body>
    <div class="red-uniform">
        <h3>Class A</h3>
        <p>Student 1</p>
        <p>Student 2</p>
        <p>Student 3</p>
        <p>Student 4</p>
    </div>

    <div class="blue-uniform">
        <h3>Class B</h3>
        <p>Student 1</p>
        <p>Student 2</p>
        <p>Student 3</p>
        <p>Student 4</p>
    </div>
</body>
</html>
```

##### **Note** 
To see the output of your selectors.html file look in https://ruchika.code.chigurucolab.com/live/selectors.html
Give a `/selectors.html` after `/live` to review the output of selectors.html file. Ofcourse replace `ruchika` with your names :P

In the above code:
- The first `<div>` gets a class name as `red-uniform`.
- The second `<div>` gets a class name as `blue-uniform`.

Let's **apply** some styles to the selectors.html file.
Write the following CSS code to index.css:
```sh
    .red-uniform{
        color: red;
    }
    .blue-uniform{
        color: blue;
    }
```
##### Output on  https://your-name.code.chigurucolab.com/live/selectors.html
****
****
![alt](https://www.linkpicture.com/q/ruchika-sc-at-25-05-2021-01-51-52.png)
****
****
We can see that class with `red-uniform` and `blue-uniform` get the respective colors as given in css. So what happend in the above code??

- The class `red-uniform` and `blue-uniform` were selected in css with a  **.**  (dot).
- Always select a class with a dot in front of the class name in CSS.
- In the example we saw how all the `<p>` elements under `red-uniforms` get text color in red and `<p>` elements under `blue-uniform` get blue color.

Then, what is an ID !!??

Copy this code to the same selector.html:
```sh
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>A school</title>
  <link rel="stylesheet" href="css/index.css">
</head>
<body>
    <div class="red-uniform">
        <h3>Class A</h3>
        <p id="green-uniform">Student 1</p>
        <p>Student 2</p>
        <p>Student 3</p>
        <p>Student 4</p>
    </div>

    <div class="blue-uniform">
        <h3>Class B</h3>
        <p>Student 1</p>
        <p>Student 2</p>
        <p>Student 3</p>
        <p>Student 4</p>
    </div>
</body>
</html>
```
- Observe the line `<p id="green-uniform">Student 1</p>`

Now, add this style into index.css:
```sh
#green-uniform {
    color: green;
}
```
##### Output on  https://your-name.code.chigurucolab.com/live/selectors.html
****
****
![alt](https://www.linkpicture.com/q/Screenshot_2021-05-25-Modern-CSS-1.png)
****
****
You should see how `Student 1` under `red-uniform` gets green color text. That's because of **Id**

- The line `<p id="green-uniform">Student 1</p>` in the code is where an id is defined to that `<p>` element.
- The rule to define an id is, `id=id_name`.
- And in the css, observe how an `#` hashtag is used to **select** an id and that `<p>` element get a green color.

> A class in CSS can be understood as a class in a school. Here we see Class A and Class B. An ID  in CSS can be understood as an individual student or roll number. That's why, a style applied to a class is common for all elements under that class. And an ID under a class just selects a single unique element and applies style only for that. Not for any thing else.

## 5. Styling a blog page

Let's deep dive into more CSS and change a boring webpage like this:
****
****
![alt](https://www.linkpicture.com/q/Screenshot_2021-05-25-Modern-CSS-2.png)
****
****
To a nice page like this:
****
****
![alt](https://www.linkpicture.com/q/Screenshot_2021-05-25-Modern-CSS-3.png)
****
****


##### Let's begin:
1] Create a new file under project as `blog.html`. Again, to see the out put of this page use, https://your-name.code.chigurucolab.com/live/blog.html
2] Copy the following html code to `blog.html`. Observe that the CSS file `index.css` is linked. 
```sh
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Modern CSS</title>
  <link rel="stylesheet" href="css/index.css">
</head>

<body>

<header>This is the header.</header>

<div class="top-image-section">
</div>

<div class="content-section">
    <h1>This is the main content.</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec gravida a metus sit amet dictum. Curabitur aliquam tincidunt ullamcorper. Morbi mattis ut ligula sit amet aliquam. Etiam sed ultricies elit. Proin pharetra hendrerit arcu, ac vestibulum arcu porttitor pretium. Nunc id ligula risus. Curabitur lacinia non erat nec hendrerit. Sed at feugiat justo. Curabitur a nisi in mauris placerat tincidunt sed ac mauris. Sed ornare imperdiet pretium. Donec ac dui et mauris dignissim placerat ac non nunc. Donec fringilla ipsum lorem. Suspendisse ex mauris, ultricies in nulla a, faucibus convallis est.</p>

    <h3>This is the second section.</h3>
    <p>Nunc lacinia ut eros vitae blandit. Pellentesque vehicula volutpat ante, placerat luctus diam lobortis vitae. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur neque nibh, ornare a lorem laoreet, sollicitudin eleifend justo. Phasellus vitae commodo dolor. Donec condimentum elit et ante ullamcorper, pellentesque viverra nibh laoreet. Nulla in hendrerit diam, eu faucibus metus. Etiam malesuada mattis lectus sed vehicula. Duis orci sem, sollicitudin non lectus vitae, auctor mollis elit. Curabitur quis faucibus eros. In sit amet neque ipsum.</p>
  
    <h4>This is an aside section.</h4>
    <p>Quisque sollicitudin metus ultrices, convallis odio eu, gravida ex. Quisque eget efficitur erat. Nulla eu erat pharetra, laoreet justo eu, aliquet lorem. Vestibulum ullamcorper, elit efficitur elementum tincidunt, sapien nisi rhoncus neque, sit amet vehicula turpis dolor ac risus. Praesent eget condimentum tortor, eu ornare elit. Maecenas quis ex eu erat semper venenatis in eget arcu. Morbi sit amet magna urna. Aenean efficitur odio eget leo eleifend interdum.</p>
</div>
<footer>This is the footer.</footer>
</body>
</html>
```
3] First,  lets apply a nice font to the text. **What is a font?** *A font is a set of printable or displayable text characters in a specific style and size.* 
```sh
html {
    font-family: Arial;
  }
```
- Selected the `html` element to apply a font as `Arial`.
- `font-family` is the property to change or set a font in css.

4] Let's change the text color, font size and body's background color and also set some padding. Copy the following code and see the difference.
```sh
  body {
    font-size: 18px;
    line-height: 1.618;
    color: blue;
    background-color: white;
    padding: 20px;
  }
```
What is padding?? Padding is used to create space around an element's content, inside of any defined borders. In our example, you should see a spacing around the body.
****
****
![alt](https://www.linkpicture.com/q/Screenshot_2021-05-25-Modern-CSS-4.png)
****
****
- `padding: 20px` applies 20 px padding on all the side i.e. top, left, right, bottom.
- CSS also has properties for specifying the padding for each side of an element:
    - `padding-top`
    - `padding-right`
    - `padding-bottom`
    - `padding-left`
- Try a padding of `  padding: 25px 50px 75px 100px;` to any element on the page. See the difference.
- The css property `line-height` sets a height or to the likes between text. The used value is this unitless number multiplied by the element's own font size. The computed value is the same as the specified number. In most cases, this is the preferred way to set line-height and avoid unexpected results due to inheritance.

5] Now, lets set a background image to the class `top-image-section`.
```sh
 .top-image-section{
    background-image: url(css/photo-1454372182658-c712e4c5a1db.jpeg);
    background-repeat: no-repeat;
    background-position: center;
    background-size: 100%;
    height: 400px;
  }
```
- We selected the class `top-image-section` with a `.`(dot) as we learnt in the previous sections.
- The css property to set a background image is `background-image`! Simple right?? Obseever the value to the property as `url()` which hold the path of my image. Here, inside css folder we have our image.
- `background-repeat: no-repeat;` is an important property. Browser loads a background image to any element multiple times. Hence, we use `no-repeat` to avoide this case.
![alt](https://www.linkpicture.com/q/Screenshot_2021-05-25-Modern-CSS-5.png)
- `background-position: center; and background-size: 100%;` are used to set the image to center and full width to be covered.
- `height` property is important otherwise we would not see the background image. Now, your output should be,
![alt](https://www.linkpicture.com/q/Screenshot_2021-05-25-Modern-CSS-7.png)

6] Let's give some `left` and `right` padding to the `content-section` class.
```sh
.content-section{
    padding-left: 300px;
    padding-right: 300px;
  }
```

7] You will find a `<footer>` tag in the bottom. This tag should be used only in the bottom for the website footer. Let's add some styles to that,
```sh
footer{
    background-color: blue;
    color: white;
    height: 100px;
    padding: 20px;
    border: solid 1px white;
  }
```
#### Combined css code is:
```sh
/* step 1 */
    html {
        font-family: Arial;
      }
  body {
    font-size: 18px;
    line-height: 1.618;
    margin: auto;
    color: blue;
    background-color: white;
    padding: 30px;
  }
  .top-image-section{
    background-image: url(css/photo-1454372182658-c712e4c5a1db.jpeg);
    background-position: center;
    background-size: 100%;
    height: 400px;
  }
  .content-section{
    padding-left: 300px;
    padding-right: 300px;
  }
  footer{
    background-color: blue;
    color: white;
    height: 100px;
    padding: 20px;
    border: solid 1px white;
  }
```

## 6. Adding a navigation bar to your blog page

A navigation looks like this. See the top of the page with a navigation bar:
![alt](https://www.linkpicture.com/q/Screenshot_2021-05-25-Modern-CSS-8.png)

Let's deal with some concepts here first. There is an other of selecting class and elements other than mentioned in section `4. Different types of selectors`. This type of selections is called `descendant combinator`. The descendant combinator — typically represented by a single space ( ) character — combines two selectors such that elements matched by the second selector are selected if they have an ancestor (parent, parent's parent, parent's parent's parent, etc) element matching the first selector. Selectors that utilize a descendant combinator are called descendant selectors. [Read this link to see other types of selections](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors#combinators). 
Example:
List items that are descendants of the "my-things" list:
```sh
ul.my-things li {
  margin: 20px;
}
```
Selects all `<p>` elements inside `<div>` elements: 
```sh
div p {
  background-color: yellow;
}
```
Add the following HTML to the `blog.html` right after `</body>` and befor`<header>:
```sh
      <ul class="nav">
        <li><a href="#">Home</a></li>
        <li><a href="#">About Us</a></li>
        <li><a href="#">Services</a></li>
        <li><a href="#">Products</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
```
And add this CSS to the end of `index.css` file:
```sh
    ul.nav {
        margin:0;
        padding:12px;
        margin-bottom: 30px;
        list-style:none;
        height:36px; 
        line-height:36px;
        background:#000000;
        font-size:18px;
    }
    ul.nav li {
        border-right:1px solid #white;
        float:left;
    }
    ul.nav a {
        display:block;
        padding:0 28px;
        color:#ccece6;
        text-decoration:none;
    }
    ul.nav a:hover,
    ul.nav li.current a {
        background:#063250;
    }
```
- `ui.nav` means, select the `<ul>` element of class name `nav`.
- `ul.nav li` means, select the '<li>' elements which are children of `<ul>` element with class name `nav`.
- `ul.nav a` means, select the `<a>` element which are children of `<ul>` element with class name `nav`.
#### Pseudo-classes
A CSS pseudo-class is a keyword added to a selector that specifies a special state of the selected element(s). For example, `:hover` can be used to change a button's color when the user's pointer hovers over it. Learn more about psedo classes [here](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-classes).
Example: Any button over which the user's pointer is hovering should text turn blue.
```sh
button:hover {
  color: blue;
}
```
Let's understand what are some other CSS properties used there:
- `list-style:none;` removes the default bullet listing to the `ul` element.
- 'border-right:1px solid #white;' sets a border to right side to `li` element.
- `float: left` this property places an element on the left side of its container, allowing text and inline elements to wrap around it. Can use `float: right` to place elements to right-side.
- `display:block;` The display CSS property sets whether an element is treated as a block or inline element and the layout used for its children. [Read this for more](https://developer.mozilla.org/en-US/docs/Web/CSS/display).
- `text-decoration:none;` removes the default underline which comes with an `<a>` element.
- `ul.nav a:hover, ul.nav li.current a`- Apply a background color to every `<a>` element mouse `hovers` on.
- `#063250;` this is a `HEX` value for a color. We use these values other than `blue` or `green` directly naming the colors. [Read here about HEX](https://htmlcolorcodes.com/)

## 7. Exercises:

##### Listed 3 levels of difficulity `Easy`, `Medium` and, `hard`. Select any one or try out all! Note that these exersises will need self-learning and research.


#### 1. Easy
- Create an event page which looks like [this](https://i.postimg.cc/L6GRBCTm/Screenshot-2021-05-25-Modern-CSS-9.png).
- You will need knowlege of all the styles we learnt like, background image, text-color, text-align etc.

#### 2. Medium
- Create a simple form similar to this: [Link](https://www.linkpicture.com/q/Screenshot_2021-05-25-Good-Vibes-Form.png)
- You will need knowledge on `<form>` elements like labels, select, option, checkbox, radio buttons. Refer to Day1's notes for basics. Also you can refer [this](https://www.tutorialspoint.com/html/html_forms.htm) 
#### 3. Difficult(not very much)
- Build a personal portfolio website. Free to use your imagination. What is expected on the page:
1] A top section  with your photo and introduction.
2] Can use good attractive colors. 
3] Give a section with a list of skills, with icons may be.
4] Finally, a contact form.
