# Web Development in Kannada - Day 1

### Today you learnt today:
1. What are browsers?
2. URL - Uniform Resource Locators
3. Basics of HTML
4. Basic of HTML tags
5. Creating your first HTML document
5. **Exersise 1**: Create a HTML document in your local computer
6. **Exersise 2**: 



## 1. What are browsers
A browser is system software that accesses and displays pages and files on the web. Browsers require a connection to the Internet (e.g., Wi-Fi). Popular web browsers include Firefox, Internet Explorer, Safari, Opera, Edge, internet explorer. Internet Explorer is only available for Windows. Safari is available for Mac OS X, iOS, and Windows.

## 2. URL - Uniform Resource Locator
URL stands for Uniform Resource Locator, and is used to specify addresses of any website on the World Wide Web.
Eg: [www.wikipedia.com](https://www.wikipedia.com) | [www.irctc.co.in](https://www.irctc.co.in)
A URL will have the following format −
```sh
protocol://hostname/other_information
```

## 3. Basics of HTML
HTML stands for Hyper Text Markup Language. HTML is a markup language that defines the structure of your content. This is the language in which we write web pages for any Website. It defines the meaning and structure of web content. Other technologies besides HTML are generally used to describe a web page's appearance/presentation (CSS) or functionality/behavior (JavaScript).

"Hypertext" refers to links that connect web pages to one another, either within a single website or between websites. Links are a fundamental aspect of the Web.

We should create a HTML page as **.html**

##### Structure of a HTML document

![alt](http://notesformsc.org/wp-content/uploads/2018/01/HTML-Structure.png)

## 4. Basics of HTML Tags
In HTML, a tag is used for creating an element. The name of an HTML element is the name used in angle brackets such as <p> for paragraph.

##### Parts of a HTML element
![](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/HTML_basics/grumpy-cat-small.png)

1. **Opening tag**: This consists of the name of the element (in this case, p), wrapped in opening and closing angle brackets. This states where the element begins or starts to take effect — in this case where the paragraph begins.
2. **The closing tag**: This is the same as the opening tag, except that it includes a forward slash before the element name. This states where the element ends — in this case where the paragraph ends. Failing to add a closing tag is one of the standard beginner errors and can lead to strange results.
3. **The content**: This is the content of the element, which in this case, is just text.
4. **The element**: The opening tag, the closing tag, and the content together comprise the element.

## 5. Creating your first HTML document

A basic structure of a HTML document is like below:
**index.html**(filename.html, don't forget ".html")

```sh
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>My test page</title>
  </head>
  <body>
    <p>Hello World!</p>
  </body>
</html>
```
1. **<!DOCTYPE html>**:  This tag specifies the language you will write on the page. In this case, the language is HTML.
2. **<html></html>**: This tag signals that from here on we are going to write in HTML code.
3. **<head></head>**: This element acts as a container for all the stuff you want to include on the HTML page that isn't the content you are showing to your page's viewers. This includes things like keywords and a page description that you want to appear in search results, CSS to style our content, character set declarations, and more.
4. **<meta charset="utf-8">**: This is where information about the document is stored: character encoding, name (page context), description. UTF stands for Unicode Transformation Format.
5. **<title></title>**: This is used to set the title of your page. See the title of your page on the tab on your browser.
5. **<body></body>**: This contains all the content that you want to show to web users when they visit your page, whether that's text, images, videos, games, playable audio tracks, or whatever else.

#### Following are the most common tag to start with
1. <p> - This is used to create a paragraph on your page.
Eg:
    ```sh
    <p>A cat belongs to the tiger family.</p>
    ```
2. <h1> to <h6> - These tags are used to add a heading element to the page.
3. <a href="link"></a> - This is a anchor link tag. Creates a hyperlink to web pages, files, email addresses, locations in the same page, or anything else a URL can address. 
    ```sh
    <a href="https://example.com">Website</a><
    ```
    **href** takes the URL that the hyperlink points to.
4. <img src="image"/> - This element embeds an image into the document.
    ```sh
    <img src="https://www.rd.com/wp-content/uploads/2021/01/GettyImages-588935825.jpg"/>
    ```
